function [kernel_mat] = CalculateRBFKernel(data,bw)
% Assume the data is in column format
bw=kernel_struct.bw;    
pdist_mat=squareform(pdist(data'));
kernel_mat=exp(-0.5*pdist_mat.^2/bw^2);
end


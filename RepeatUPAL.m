NUM_REPEATS=5;
lossstr='logistic';
BUDGET=300;

[temp1, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    basepath='/home/gmravi/';
else
    basepath='/net/hu17/gmravi/';
end

dirpath=strcat(basepath,'matlab_codes/iwal/whitewine/');
trn_data_file=strcat(dirpath,'whitewine_train_data.txt');
tst_data_file=strcat(dirpath,'whitewine_test_data.txt');
trn_labels_file=strcat(dirpath,'whitewine_train_labels.txt');
tst_labels_file=strcat(dirpath,'whitewine_test_labels.txt');
strategy_upal='old';
lambda_upal=10^-2;
explrexpupal=1;
[xtrn,xtst,ytrn,ytst]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);

numtrn=size(xtrn,2);
numtst=size(xtst,2);
trn_data=[xtrn;ones(1,numtrn)];
tst_data=[xtst;ones(1,numtst)];
numdims=size(xtrn,1);


% The data is arranged column wise. Hence the data is d x n
% d= num of dimensions, n=num of points.

tst_err_mat=zeros(BUDGET,NUM_REPEATS);    
for repeat_num=1:NUM_REPEATS %For each repetition
    stream = RandStream('mt19937ar','Seed',sum(100*clock));
    UPAL;  
    tst_err_mat(:,repeat_num:repeat_num)=tsterrupalqrs;
end
avg_tst_err_vec=mean(tst_err_mat,2);
std_tst_err_vec=std(tst_err_mat');
cum_avg_tst_err=sum(avg_tst_err_vec);
cum_std_tst_err=sum(std_tst_err_vec);
save(strcat(basepath,['matlab_codes/VC_UPAL/expt_results/whitewine/' ...
                  'upal_results.mat']));
display('Average test error after BUDGET');
display(avg_tst_err_vec(end));
display('Cumulative avg tst err rate is...');
display(cum_avg_tst_err);
% Make plot

mnist_flag=true;
statlog_flag=true;
whitewine_flag=true;
abalone_flag=true;

if(mnist_flag)
    clearvars -except *_flag;
    datapath='~/matlab_codes/iwal/mnist/logistic_loss/fold_0_average_results_mnist.mat';
    figpath='~/matlab_codes/iwal/mnist/logistic_loss/fold_0_average_results_mnist_no_vw';
    load(datapath);
    clf;

    axes('Linewidth',3,'Box','on','FontSize',18);
    hold all;
    
    %UPAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_poolal_queries),'-','Linewidth',4);

    %BMAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_bmal_queries),'--','Linewidth',5);

    %VW
    %    plot([1:BUDGET],100*smooth(fold0avgtsterror_vw(1:BUDGET)),'-.','Linewidth',3);

    %RAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_rand_queries),'--','Linewidth',4);

    %Pass
    plot([1:BUDGET],100*smooth(fold0avgtsterror_pass_queries(1:BUDGET)),'--+','Linewidth',1);

    ymin=4;
    ymax=30;
    ylim([ymin,ymax]);
    legend('UPAL','BMAL','RAL','PL');
    grid on;
    set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
    
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [7.00 7.00]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 7.00 7.00]);
    
    saveas(gcf,[figpath,'.pdf']);
end
if(statlog_flag)
    clearvars -except *_flag;
    datapath='~/matlab_codes/iwal/statlog/logistic_loss/fold_0_average_results_statlog.mat';
    figpath='~/matlab_codes/iwal/statlog/logistic_loss/fold_0_average_results_statlog_no_vw';
    load(datapath);
    clf;
    axes('Linewidth',3,'Box','on','FontSize',18);
    
    hold all;
    %UPAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_poolal_queries),'-','Linewidth',4);

    %BMAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_bmal_queries),'--','Linewidth',5);

    %VW
    %    plot([1:BUDGET],100*smooth(fold0avgtsterror_vw(1:BUDGET)),'-.','Linewidth',3);

    %RAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_rand_queries),'--','Linewidth',4);

    %Pass
    plot([1:BUDGET],100*smooth(fold0avgtsterror_pass_queries(1:BUDGET)),'--+','Linewidth',1);

    ymin=2;
    ymax=30;
    ylim([ymin,ymax]);
    legend('UPAL','BMAL','RAL','PL');
    grid on;
    set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [7.00 7.00]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 7.00 7.00]);
    
    saveas(gcf,[figpath,'.pdf']);
end
if(whitewine_flag)
    clearvars -except *_flag;
    datapath='~/matlab_codes/iwal/whitewine/logistic_loss/fold_0_average_results_whitewine.mat';
    figpath='~/matlab_codes/iwal/whitewine/logistic_loss/fold_0_average_results_whitewine_no_vw';
    
    load(datapath);
    clf;
    
    axes('Linewidth',3,'Box','on','FontSize',18);
    
    hold all;
    %UPAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_poolal_queries),'-','Linewidth',4);

    %BMAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_bmal_queries),'--','Linewidth',5);

    %VW
    %    plot([1:BUDGET],100*smooth(fold0avgtsterror_vw(1:BUDGET)),'-.','Linewidth',3);

    %RAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_rand_queries),'--','Linewidth',4);

    %Pass
    plot([1:BUDGET],100*smooth(fold0avgtsterror_pass_queries(1:BUDGET)),'--+','Linewidth',1);

    ymin=23;
    ymax=40;
    ylim([ymin,ymax]);
    legend('UPAL','BMAL','RAL','PL');
    grid on;
    set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [7.00 7.00]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 7.00 7.00]);
    

    saveas(gcf,[figpath,'.pdf']);
end


if(abalone_flag)
    clearvars -except *_flag;
    datapath='~/matlab_codes/iwal/abalone/logistic_loss/fold_0_average_results_abalone.mat';
    figpath='~/matlab_codes/iwal/abalone/logistic_loss/fold_0_average_results_abalone_no_vw';
    load(datapath);
    clf;
    
    axes('Linewidth',3,'Box','on','FontSize',18);
    
    hold all;
    %UPAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_poolal_queries),'-','Linewidth',4);

    %BMAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_bmal_queries),'--','Linewidth',5);

    %VW
    %    plot([1:BUDGET],100*smooth(fold0avgtsterror_vw(1:BUDGET)),'-.','Linewidth',3);

    %RAL
    plot([1:BUDGET],100*smooth(fold0avgtsterror_rand_queries),'--','Linewidth',4);

    %Pass
    plot([1:BUDGET],100*smooth(fold0avgtsterror_pass_queries(1:BUDGET)),'--+','Linewidth',1);

    ymin=23;
    ymax=50;
    ylim([ymin,ymax]);
    legend('UPAL','BMAL','RAL','PL');
    grid on;
    set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [7.00 7.00]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 7.00 7.00]);
    

    saveas(gcf,[figpath,'.pdf']);
end
